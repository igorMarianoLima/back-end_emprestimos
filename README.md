# Desafio Back-End | Empréstimos

Este repositório é uma solução para o desafio back-end "Empréstimos" (Que pode ser encontrado [aqui](https://github.com/backend-br/desafios/blob/master/loans/PROBLEM.md)), ao qual se deve implementar um serviço que retorne as modalidades de empréstimos disponíveis para um determinado cliente, com base nos seguintes atributos: **Idade**, **Salário** e **Localização**. 
A solução proposta foi desenvolvida em Nest.Js, utilizando conceitos como validações, DTOs e boas práticas de desenvolvimento.