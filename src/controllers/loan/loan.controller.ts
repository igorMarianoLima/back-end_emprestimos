import { Body, Controller, Post } from '@nestjs/common';

import { LoanService } from 'src/services/loan/loan.service';

import { GetCustomerLoanDto } from 'src/dtos/loan/get-customer-loan.dto';

@Controller('')
export class LoanController {
  constructor(private loanService: LoanService) {}

  @Post('customer-loans')
  async getCustomerLoans(@Body() customer: GetCustomerLoanDto) {
    return this.loanService.getCustomerLoans(customer);
  }
}
