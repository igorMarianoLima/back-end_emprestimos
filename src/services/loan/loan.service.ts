import { Injectable } from '@nestjs/common';

import { SendCustomerLoansDto } from 'src/dtos/loan/send-customer-loans.dto';

import { CustomerModel } from 'src/models/customer/customer.model';
import { ConsignmentLoanModel } from 'src/models/loan/consignmentLoan.model copy';
import { GuaranteedLoanModel } from 'src/models/loan/guaranteedLoan.model copy';
import { PersonalLoanModel } from 'src/models/loan/personalLoan.model';

@Injectable()
export class LoanService {
  getCustomerLoans(customer: CustomerModel): SendCustomerLoansDto {
    const types: SendCustomerLoansDto['loans'] = [];

    if (customer.income <= 3000) {
      types.push(new PersonalLoanModel());
      types.push(new GuaranteedLoanModel());
    } else if (customer.income < 5000) {
      if (customer.age < 30 && customer.location.toUpperCase() === 'SP') {
        types.push(new PersonalLoanModel());
        types.push(new GuaranteedLoanModel());
      }
    } else {
      types.push(new ConsignmentLoanModel());
    }

    return {
      customer: customer.name,
      loans: types,
    };
  }
}
