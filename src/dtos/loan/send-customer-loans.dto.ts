import { CustomerModel } from 'src/models/customer/customer.model';
import { LoanModel } from 'src/models/loan/loan.model';

export class SendCustomerLoansDto {
  customer: CustomerModel['name'];
  loans: {
    type: string;
    interest_rate: LoanModel['interest_rate'];
  }[];
}
