import {
  IsInt,
  Min,
  IsTaxId,
  Max,
  Matches,
  IsString,
  IsNumber,
  Length,
} from 'class-validator';

export class GetCustomerLoanDto {
  @IsInt({
    message: 'Idade inválida',
  })
  @Min(0, {
    message: 'Idade inválida',
  })
  @Max(120, {
    message: 'Idade inválida',
  })
  age: number;

  @IsTaxId('pt-BR', {
    message: 'CPF inválido',
  })
  cpf: string;

  @IsString()
  @Matches(/^[a-zà-ú ]+$/gi, {
    message: 'Nome inválido',
  })
  name: string;

  @IsNumber()
  income: number;

  @IsString()
  @Length(2, 2)
  location: string;
}
