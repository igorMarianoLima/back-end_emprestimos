import { LoanModel } from './loan.model';

export class PersonalLoanModel extends LoanModel {
  constructor() {
    super();

    this.type = 'PERSONAL';
    this.interest_rate = 4;
  }
}
