import { LoanModel } from './loan.model';

export class ConsignmentLoanModel extends LoanModel {
  constructor() {
    super();

    this.type = 'CONSIGNMENT';
    this.interest_rate = 2;
  }
}
