export abstract class LoanModel {
  type: string;
  interest_rate: number;

  constructor() {
    this.type = '';
    this.interest_rate = 0;
  }
}
