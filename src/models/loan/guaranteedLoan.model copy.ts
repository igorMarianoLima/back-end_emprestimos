import { LoanModel } from './loan.model';

export class GuaranteedLoanModel extends LoanModel {
  constructor() {
    super();

    this.type = 'GUARANTEED';
    this.interest_rate = 3;
  }
}
