export class CustomerModel {
  age: number;
  cpf: string;
  name: string;
  income: number;
  location: string;
}
